##################
#
# VPC definition
#
##################

# create the VPC
resource "aws_vpc" "Pomelo_VPC" {
  cidr_block           = var.vpcCIDRblock
  instance_tenancy     = var.instanceTenancy 
  enable_dns_support   = var.dnsSupport 
  enable_dns_hostnames = var.dnsHostNames
  tags = {
    Name        = var.vpc_name
    creator     = var.vpc_creator
    region      = var.vpc_region
    environment = var.vpc_env
    atomation   = var.vpc_atomation
    account     = var.account_id
  }
}

output "vpc_id" {
  value = aws_vpc.Pomelo_VPC.id
}


################
# Public subnet
################

resource "aws_subnet" "Pomelo_public" {
  vpc_id                  = aws_vpc.Pomelo_VPC.id
  cidr_block              = var.publicsubnetCIDRblock
  map_public_ip_on_launch = var.mapPublicIP 
  availability_zone       = var.availabilityZone
  
  tags = {
    Name              = var.public_subnet_name
    
  }
}

output "public_subnet_id" {
  value = aws_subnet.Pomelo_public.id
}
